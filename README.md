## Community Configuration 2.0 Usage Example

The goal of this extension is to provide valid examples of usages of
the Community Configuration extension. For instructions on how to set
up, please see https://gitlab.wikimedia.org/repos/growth/community-configuration.
